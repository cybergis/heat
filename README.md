# Heat Orchestration Templates (HOT)


## Usage

1. Choose __+ Launch Stack__ from the Orchestration menu 
2. Choose __URL__ from the *Template Source* dropdown
3. Added the URL of Heat template to the *Template URL* field

Any URL may be used. To use template from this repository, navigate to 
the template file within the source browser, copy the URL from the address 
bar, and paste it into the *Template URL* field.
